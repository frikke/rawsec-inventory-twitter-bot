#!/usr/bin/env ruby

# Ruby internal
require 'net/https'
# External
require 'twitter'
require 'oj'

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = ENV['twitter_consumer_key']
  config.consumer_secret     = ENV['twitter_consumer_secret']
  config.access_token        = ENV['twitter_access_token']
  config.access_token_secret = ENV['twitter_access_token_secret']
end

# API's constant
api_base_url = 'https://inventory.raw.pm/api/'.freeze
api_root_endpoint = 'api.json'.freeze

# twitter max message size
tweet_max = 280

# API root URI
uri = URI(api_base_url + api_root_endpoint)
# Get the JSON
res = Net::HTTP.get_response(uri)

if res.is_a?(Net::HTTPSuccess)
  # prepare for parsing
  doc = Oj::Doc.open(res.body)
  # parse (JSON string to ruby object)
  json_obj = doc.fetch('/')
  # get categories
  categories = json_obj.keys
  # peudo-randomly pick a category
  category = categories.sample
  # get sub categories
  json_obj = doc.fetch("/#{category}")
  sub_categories = json_obj.keys
  # peudo-randomly pick a sub category
  sub_category = sub_categories.sample
  # get item type and number of items
  item_type = category
  item_number = doc.fetch("/#{category}/#{sub_category}/items")
  # peudo-randomly pick a tool or resource
  prng = Random.new
  prn = prng.rand(item_number) + 1
  picked_item = doc.fetch("/#{category}/#{sub_category}/#{item_type}/#{prn}")

  # format ouput (long format)
  message = "Tool / ressource of the day\n\n"
  picked_item.each do |key, value|
    if value.is_a?(String)
      message += "#{key}: #{value}\n"
    else
      message += "#{key}: \n"
      value.each do |item|
        message += "  - #{item.values.first}\n"
      end
    end
  end

  cropped = "\n[cropped]\n"
  reference = "\nFind out more on ".concat(api_base_url.chomp('api/'))
  # maximum cropped message size
  c_max = tweet_max - reference.length - cropped.size
  # maximum cropped message + cropped tag size
  ct_max = tweet_max - reference.length
  # check if message is smaller than twitter max allowed size
  # keep room for the reference and cropped tag
  message = message[0..c_max] + cropped if message.length > ct_max

  # make reference
  message += reference

  # tweet the message
  if ENV['TWITTER_BOT_ENV'] == 'tweet'
    client.update(message)
  elsif ENV['TWITTER_BOT_ENV'] == 'test'
    puts message
  else
    raise 'No environment defined'
  end
end
