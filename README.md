# Rawsec Inventory Twitter Bot

The bot is randomly picking a tool or resource at https://inventory.raw.pm/api/api.json and publishing it (in my case every day).

This is a one time task that need to be run periodically by a cron job.

This project can easily be modified to pick other stuff from other API and use other time frequency.

+ Twitter url: https://twitter.com/RawsecBot
+ Gitlab url: https://gitlab.com/rawsec/rawsec-inventory-twitter-bot/
+ Heroku (private) url: https://dashboard.heroku.com/apps/rawsec-inventory-twitter-bot

## Setup

### Environment variables

Environment variables in your deployment environment (ex: Heroku Variables, GitLab CI Variables).

Those value are stored in the **Keys and Access Tokens** tab of https://apps.twitter.com/.

+ `twitter_consumer_key` for *Consumer Key (API Key)*
+ `twitter_consumer_secret` for *Consumer Secret (API Secret)*
+ `twitter_access_token` for *Access Token*
+ `twitter_access_token_secret` for *Access Token Secret*

## Examples

### Heroku + cron-job.org

This is en example where the app is hosted on [Heroku](heroku.com) and where ephemeral dynos are triggered with [Heroku Platform API](https://devcenter.heroku.com/articles/platform-api-reference#dyno) and [cron-job.org](https://cron-job.org/). We are using external cron job in order not to pay *Custom Clock Processes* or to set billing address for [Heroku Scheduler](https://elements.heroku.com/addons/scheduler) add-on.

Install the Heroku CLI tool: [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli).

Then [login and initiate your repo](https://devcenter.heroku.com/articles/getting-started-with-ruby#set-up) and create auth API token `heroku authorizations:create` (one time).

Set the environment variables in the app *Settings* > *Config Vars*.

Create one-off dyno that can be called by a free web cron on [cron-job.org](https://cron-job.org/):

+ Url: `https://api.heroku.com/apps/rawsec-inventory-twitter-bot/dynos`
+ Schedule: every day at 8am
+ Advanced:
  - Request method: POST
  -  Custom headers:
    * `Content-Type`: `application/json`
    * `Accept`: `application/vnd.heroku+json; version=3`
    * `Authorization`: `Bearer $token` where `$token` is get from `heroku authorizations:create`
  -  Request body:
  ```
  {
    "attach": false,
    "command": "bundle exec ruby ./twitterBot.rb",
    "size": "Free",
    "type": "run",
    "time_to_live": 60
  }
  ```

The equivalent curl command is:

```
$ curl -X POST https://api.heroku.com/apps/rawsec-inventory-twitter-bot/dynos \
-d '{
  "attach": false,
  "command": "bundle exec ruby ./twitterBot.rb",
  "size": "Free",
  "type": "run",
  "time_to_live": 60
}' \
  -H "Content-Type: application/json" \
  -H "Accept: application/vnd.heroku+json; version=3" \
  -H "Authorization: Bearer $token"
```

### GitLab

This is en example where the app is hosted [GitLab](https://gitlab.com) and where the bot is run by GitLab CI and orchestrated by [GitLab CD Scheduled Pipelines](https://gitlab.com/help/user/project/pipelines/schedules).

Create the `.gitlab-ci.yml` file or use the existing one (see in the git repo).

Set the environment variables in the repository *Settings* > *CI/CD* > *Variables*.

Create the scheduled task:
  + You can choose the *Interval Pattern*
  + You can choose the *Cron Timezone*
  + Set the *Target Branch* to `master`
  + In *Activated* check the *Active* box
